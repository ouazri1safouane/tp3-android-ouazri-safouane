package fr.uavignon.ceri.tp3.data.webservice;

public class Forecast {
    public final String name;
    public final int temperature;
    public final String description;
    public final String icon;

    public Forecast(String name, int temperature,
                    String description, String icon) {
        this.name = name;
        this.temperature = temperature;
        this.description = description;
        this.icon = icon;
    }
}
