package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface  OWMInterface {
    @Headers("Accept: application/geo+json")
    @GET("/weather")
    Call<WeatherResponse> getForecast(@Query("q") String q,
                                      @Query("appid") String office);
}
