package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public final boolean isLoading;
    public final List<Forecast> forecasts;
    public final Throwable error;

    WeatherResult(boolean isLoading, List<Forecast> forecasts, Throwable error) {
        this.isLoading = isLoading;
        this.forecasts = forecasts;
        this.error = error;
    }



    void transferInfo(WeatherResponse weatherInfo, City cityInfo){
      weatherInfo.properties.periods ;

    }
}
